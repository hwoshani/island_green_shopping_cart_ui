import React, {Component} from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import Header from "./components/Header";
import Products from "./components/Products";
import Landing from "./components/Landing";
import Footer from "./components/Footer";
import QuickView from "./components/QuickView";
import 'bootstrap/dist/css/bootstrap.css';
import  ProductDetails from './components/ProductDetails'
import "./scss/style.scss";
import {BrowserRouter, Route} from "react-router-dom";
import ProceedToCheckout from "./components/ProceedToCheckout";

class App extends Component {
    constructor() {
        super();
        this.state = {
            cart: [],
            totalItems: 0,
            totalAmount: 0,
            products: [],
            term: "",
            quantity: 1,
            category: "",
            cartBounce: false,
            quickViewProduct: {},
            modalActive: false
        };
        this.handleSearch = this.handleSearch.bind(this);
        this.handleMobileSearch = this.handleMobileSearch.bind(this);
        this.handleCategory = this.handleCategory.bind(this);
        this.handleAddToCart = this.handleAddToCart.bind(this);
        this.sumTotalItems = this.sumTotalItems.bind(this);
        this.sumTotalAmount = this.sumTotalAmount.bind(this);
        this.checkProduct = this.checkProduct.bind(this);
        this.updateQuantity = this.updateQuantity.bind(this);
        this.handleRemoveProduct = this.handleRemoveProduct.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    // Fetch Initial Set of Products from external API
    getProducts() {
        let url =
            "http://192.168.8.110:8080/batch-service/get-all-batches";
        axios.get(url).then(response => {
            this.setState({
                products: response.data
            });
            // console.log(this.state.products);
        });
    }

    componentWillMount() {
        this.getProducts();
    }

    // Search by Keyword
    handleSearch(event) {
        this.setState({term: event.target.value});
    }

    // Mobile Search Reset
    handleMobileSearch() {
        this.setState({term: ""});
    }

    // Filter by Category
    handleCategory(event) {
        this.setState({category: event.target.value});
        console.log(this.state.category);
    }

    // Add to Cart
    handleAddToCart(selectedProducts) {
        let cartItem = this.state.cart;
        let productID = selectedProducts.id;
        let productQty = selectedProducts.quantity;
        if (this.checkProduct(productID)) {
            console.log("hi");
            let index = cartItem.findIndex(x => x.id == productID);
            cartItem[index].quantity =
                Number(cartItem[index].quantity) + Number(productQty);
            this.setState({
                cart: cartItem
            });
        } else {
            cartItem.push(selectedProducts);
        }
        this.setState({
            cart: cartItem,
            cartBounce: true
        });
        setTimeout(
            function () {
                this.setState({
                    cartBounce: false,
                    quantity: 1
                });
                console.log(this.state.quantity);
                console.log(this.state.cart);
            }.bind(this),
            1000
        );
        this.sumTotalItems(this.state.cart);
        this.sumTotalAmount(this.state.cart);
    }

    handleRemoveProduct(id, e) {
        let cart = this.state.cart;
        let index = cart.findIndex(x => x.id == id);
        cart.splice(index, 1);
        this.setState({
            cart: cart
        });
        this.sumTotalItems(this.state.cart);
        this.sumTotalAmount(this.state.cart);
        e.preventDefault();
    }

    checkProduct(productID) {
        let cart = this.state.cart;
        return cart.some(function (item) {
            return item.id === productID;
        });
    }

    sumTotalItems() {
        let total = 0;
        let cart = this.state.cart;
        total = cart.length;
        this.setState({
            totalItems: total
        });
    }

    sumTotalAmount() {
        let total = 0;
        let cart = this.state.cart;
        for (var i = 0; i < cart.length; i++) {
            total += cart[i].price * parseInt(cart[i].quantity);
        }
        this.setState({
            totalAmount: total
        });
    }

    //Reset Quantity
    updateQuantity(qty) {
        console.log("quantity added...");
        this.setState({
            quantity: qty
        });
    }

    // Open Modal
    openModal(product) {
        this.setState({
            quickViewProduct: product,
            modalActive: true
        });
    }

    // Close Modal
    closeModal() {
        this.setState({
            modalActive: false
        });
    }

    render() {
        return (
            <BrowserRouter>
                <div className="containe">
                    <Header
                        cartBounce={this.state.cartBounce}
                        total={this.state.totalAmount}
                        totalItems={this.state.totalItems}
                        cartItems={this.state.cart}
                        removeProduct={this.handleRemoveProduct}
                        handleSearch={this.handleSearch}
                        handleMobileSearch={this.handleMobileSearch}
                        handleCategory={this.handleCategory}
                        categoryTerm={this.state.category}
                        updateQuantity={this.updateQuantity}
                        productQuantity={this.state.moq}
                    />

                    <Route exact path="/" render={(props) => <Landing {...props} productsList={this.state.products}
                                                                      searchTerm={this.state.term}
                                                                      productQuantity={this.state.quantity}
                                                                      addToCart={this.handleAddToCart}
                                                                      updateQuantity={this.updateQuantity}
                                                                      openModal={this.openModal}
                                                                      product={this.state.quickViewProduct}
                                                                      openModalQuick={this.state.modalActive}
                                                                      closeModal={this.closeModal}/>
                    }

                    />
                    <Route path="/proceedToCheckout"
                           render={(props) => <ProceedToCheckout {...props} cartItems={this.state.cart}
                                                                 updateQuantity={this.updateQuantity}
                                                                 removeProduct={this.handleRemoveProduct}
                                                                 total={this.state.totalAmount}/>
                           }/>


                    <Route path="/shopItem/:id"
                           component= {ProductDetails} />
                    {/*<Products*/}
                    {/*  productsList={this.state.products}*/}
                    {/*  searchTerm={this.state.term}*/}
                    {/*  addToCart={this.handleAddToCart}*/}
                    {/*  productQuantity={this.state.quantity}*/}
                    {/*  updateQuantity={this.updateQuantity}*/}
                    {/*  openModal={this.openModal}*/}
                    {/*/>*/}
                    <Footer/>
                    {/*<QuickView*/}
                    {/*    product={this.state.quickViewProduct}*/}
                    {/*    openModal={this.state.modalActive}*/}
                    {/*    closeModal={this.closeModal}*/}
                    {/*/>*/}
                </div>
            </BrowserRouter>
        );
    }
}

ReactDOM.render(<App/>, document.getElementById("root"));
