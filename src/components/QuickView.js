  import React, { Component } from "react";
  import ReactDOM,{ findDOMNode } from "react-dom";
  import "react-responsive-carousel/lib/styles/carousel.min.css";
  import { Carousel } from 'react-responsive-carousel';

  class QuickView extends Component {
    constructor(props) {
      super(props);
      this.state = {
        images: this.props.product.images
      }
    }
    componentDidMount() {
      document.addEventListener(
        "click",
        this.handleClickOutside.bind(this),
        true
      );
    }

    componentWillUnmount() {
      document.removeEventListener(
        "click",
        this.handleClickOutside.bind(this),
        true
      );
    }

    handleClickOutside(event) {
      const domNode = findDOMNode(this.refs.modal);
      if (!domNode || !domNode.contains(event.target)) {
        this.props.closeModal();
      }
    }

    handleClose() {
      this.props.closeModal();
    }

    render() {
      return (
        <div
          className={
            this.props.openModal ? "modal-wrapper active" : "modal-wrapper "
          }
        >
          <div className="modal" ref="modal">
            <button
                type="button"
                className="close"
                onClick={this.handleClose.bind(this)}
            >
              &times;
            </button>
            <div className="quick-view">
              <div className="quick-view-image">

                {
                  this.props.openModal ? <Carousel >
                        {this.props.product.images.map(image =>
                        (<div key={image.id}>
                          <img src={image.path} />
                        </div>)

                     )}</Carousel> : "no image "
                }

              </div>
              <div className="quick-view-details">
                <span className="product-name">{this.props.product.name}</span>
                <br />
                <span className="product-quantity">{this.props.product.quantity}</span>
                {/*<span className="product-name">{this.props.product.name}</span>*/}

                <span className="product-price"> {this.props.product.price}</span>
              </div>

              </div>
            </div>
          </div>

      );
    }
  }

  export default QuickView;
