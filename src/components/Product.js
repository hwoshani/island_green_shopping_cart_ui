import React, { Component } from "react";
import { Link } from 'react-router-dom';
import Counter from "./Counter";

class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedProduct: {},
      quickViewProdcut: {},
      isAdded: false
    };
  }
  addToCart(images, name, price, id, quantity) {
    this.setState(
      {
        selectedProduct: {
          image: images,
          name: name,
          price: price,
          id: id,
          quantity: quantity
        }
      },
      function() {
        this.props.addToCart(this.state.selectedProduct);
      }
    );
    this.setState(
      {
        isAdded: true
      },
      function() {
        setTimeout(() => {
          this.setState({
            isAdded: false,
            selectedProduct: {}
          });
        }, 3500);
      }
    );
  }
  // quickView(images, name, price, id, quantity) {
  //   this.setState(
  //     {
  //       quickViewProduct: {
  //         images: images,
  //         name: name,
  //         price: price,
  //         id: id,
  //         quantity: quantity
  //       }
  //     },
  //     function() {
  //       this.props.openModal(this.state.quickViewProduct);
  //     }
  //   );
  // }
  render() {
    let images = this.props.images;
    let name = this.props.name;
    let price = this.props.price;
    let id = this.props.id;
    let quantity = this.props.productQuantity;
    return (
      <div className="product">
        <div className="product-image">
          <img
            src={images[0].path}
            alt={this.props.name}
            // onClick={this.quickView.bind(
            //   this,
            //   images,
            //   name,
            //   price,
            //   id,
            //   quantity
            // )}
          />
        </div>
        <h4 className="product-name">{this.props.name} - { this.props.unitSize } {this.props.quantityType}</h4>
        <p className="product-price">{this.props.price} </p>
        {/*<Counter*/}
        {/*  productQuantity={quantity}*/}
        {/*  updateQuantity={this.props.updateQuantity}*/}
        {/*  resetQuantity={this.resetQuantity}*/}
        {/*/>*/}
        <div className="product-action">
          <Link to={'/shopItem/'+ this.props.id }  >
          <button
            // className={!this.state.isAdded ? "" : "added"}
            // type="button"
            // onClick={this.addToCart.bind(
            //   this,
            //   images[0].path,
            //   name,
            //   price,
            //   id,
            //   quantity
            // )}
          >
            Shop Now
            {/*{!this.state.isAdded ? "ADD TO CART" : "✔ ADDED"}*/}
          </button>
          </Link>
        </div>
      </div>
    );
  }
}

export default Product;
