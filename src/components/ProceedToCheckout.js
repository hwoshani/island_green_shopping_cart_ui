import React, {Component} from 'react';
// import "../scss/style.scss";
import EmptyCart from "../empty-states/EmptyCart";
import CSSTransitionGroup from "react-transition-group/CSSTransitionGroup";
import CartScrollBar from "./CartScrollBar";
import Counter from "./Counter";

class ProceedToCheckout extends Component {

    constructor(props) {
        super(props);
        this.state = {

            cartItems: this.props.cartItems,

        }
    }

    render() {

        let cartItems;
        cartItems = this.state.cartItems.map(product => {
            return (
                <li className="cart-item" key={product.name}>
                    <img className="product-image" src={product.image}/>
                    <div className="product-info">
                        <p className="product-name">{product.name}</p>
                        <p className="product-price">{product.price}</p>
                    </div>
                    {/*<Counter*/}
                    {/*    productQuantity={product.quantity}*/}
                    {/*    updateQuantity={this.props.updateQuantity}*/}
                    {/*    resetQuantity={this.resetQuantity}*/}
                    {/*/>*/}
                    <div className="product-total">
                        <p className="quantity">
                            {product.quantity} {product.quantity > 1 ? "Nos." : "No."}{" "}
                        </p>
                        <p className="amount">{product.quantity * product.price}</p>
                    </div>
                    <a
                        className="product-remove"
                        href="#"
                        onClick={this.props.removeProduct.bind(this, product.id)}
                    >
                        ×
                    </a>
                </li>
            );
        });
        let view;
        if (cartItems.length <= 0) {
            view = <EmptyCart/>;
        } else {
            view = (
                <CSSTransitionGroup
                    transitionName="fadeIn"
                    transitionEnterTimeout={500}
                    transitionLeaveTimeout={300}
                    component="ul"
                    className="cart-items-checkout"
                >
                    {cartItems}
                </CSSTransitionGroup>
            );
        }
        return (
            <header>
                <div className="container row">
                    <div className=" col-sm-3">
                        <CartScrollBar>
                            {view}
                        </CartScrollBar>
                    </div>
                    <div className="col-sm-6">
                        {this.props.total}
                    </div>
                </div>
            </header>

        );
    }
}

export default ProceedToCheckout;