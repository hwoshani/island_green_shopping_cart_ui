import { BrowserRouter, Route } from "react-router-dom";
import React, {Component} from 'react';
import ProceedToCheckout from "./components/ProceedToCheckout";

class Routes extends Component {
    render() {
        return (
            <BrowserRouter>
                <div>
                    <Route exact path="/proceedToCheckout" component={ProceedToCheckout} />
                </div>
            </BrowserRouter>
        );
    }
}

export default Routes;